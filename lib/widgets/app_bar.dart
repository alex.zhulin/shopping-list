
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shopping_list/constants/colors.dart';

class CustomAppBar extends PreferredSize {
  final double height = 120;
  final String title;

  CustomAppBar({this.title});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: preferredSize.height,
      alignment: Alignment.center,
      child: Text(
        title,
        style: TextStyle(color: Colors.white, fontSize: 23),
      ),
      decoration: BoxDecoration(
        color: Color(mainColor),
        boxShadow: [new BoxShadow(blurRadius: 30.0)],
        borderRadius: new BorderRadius.vertical(
          bottom: Radius.elliptical(MediaQuery
              .of(context)
              .size
              .width, 150.0),
        ),
      ),
    );
  }
}