import 'package:flutter/material.dart';
import 'package:shopping_list/constants/colors.dart';
import 'package:shopping_list/models/shopping_item.dart';
import 'package:shopping_list/services/app_localization.dart';
import 'package:shopping_list/services/db_service.dart';

class ItemWidget extends StatefulWidget {
  ShoppingItem item;
  final VoidCallback itemDeleted;
  final VoidCallback itemInCart;

  TextEditingController _descriptionController = TextEditingController();

  bool inEditState = false;

  ItemWidget({Key key, this.item, this.itemDeleted, this.itemInCart})
      : super(key: key);

  @override
  _ItemWidgetState createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<ItemWidget> {


  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
        side: BorderSide(color: _getWidgetColor(), width: 2),
      ),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Text(
                widget.item.name,
                style: TextStyle(
                  color: _getWidgetColor(),
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  decoration:
                      widget.item.inCart ? TextDecoration.lineThrough : null,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: widget.inEditState
                  ? TextFormField(
                      controller: widget._descriptionController,
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context)
                            .translate("description"),
                        border: OutlineInputBorder(),
                      ),
                      textCapitalization: TextCapitalization.sentences,
                      minLines: 3,
                      maxLines: 20,
                      autofocus: true,
                    )
                  : widget.item.description.trim() != ""
                      ? Text(
                          AppLocalizations.of(context)
                                  .translate("description") +
                              ": " +
                              widget.item.description.trim(),
                          style: TextStyle(color: Color(mainColor)))
                      : null,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  RawMaterialButton(
                    onPressed: () => widget.itemDeleted(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    child: Image(
                      image: widget.item.inCart
                          ? AssetImage('assets/images/trash-grey.png')
                          : AssetImage('assets/images/trash.png'),
                      height: 30,
                    ),
                    padding: EdgeInsets.all(18.0),
                    shape: CircleBorder(),
                  ),
                  RawMaterialButton(
                    onPressed: () => widget.itemInCart(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    child: Image(
                      image: !widget.item.inCart
                          ? AssetImage('assets/images/cart.png')
                          : AssetImage('assets/images/ok.png'),
                      height: 30,
                    ),
                    padding: EdgeInsets.all(18.0),
                    shape: CircleBorder(),
                  ),
                  RawMaterialButton(
                    onPressed: () => {
                      setState(() {
                        if (widget.inEditState) {
                          ShoppingItem item = ShoppingItem(
                              id: widget.item.id,
                              name: widget.item.name,
                              description: widget._descriptionController.text.trim(),
                              inCart: widget.item.inCart,
                              inList: widget.item.inList
                          );
                          DbService().insertShoppingItem(item);
                          widget.item = item;
                        } else {
                          widget._descriptionController.text = widget.item.description;
                        }
                        widget.inEditState = !widget.inEditState;
                      })
                    },
                    elevation: 2.0,
                    fillColor: Colors.white,
                    child: Image(
                      image: !widget.inEditState
                          ? AssetImage('assets/images/edit.png')
                          : AssetImage('assets/images/save.png'),
                      height: 30,
                    ),
                    padding: EdgeInsets.all(18.0),
                    shape: CircleBorder(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Color _getWidgetColor() {
    return widget.item.inCart ? Color(greyColor) : Color(mainColor);
  }

  @override
  void initState() {
    super.initState();
  }
}
