import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:shopping_list/constants/colors.dart';
import 'package:shopping_list/models/shopping_item.dart';
import 'package:shopping_list/services/app_localization.dart';
import 'package:shopping_list/services/db_service.dart';

import 'app_bar.dart';

class NewItem extends StatelessWidget {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 30, 10, 8),
              child: TextFormField(
                controller: _nameController,
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context).translate("name"),
                  border: OutlineInputBorder(),
                ),
                textCapitalization: TextCapitalization.sentences,
                autofocus: true,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: _descriptionController,
                decoration: InputDecoration(
                  labelText:
                      AppLocalizations.of(context).translate("description"),
                  border: OutlineInputBorder(),
                ),
                textCapitalization: TextCapitalization.sentences,
                minLines: 3,
                maxLines: 20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: Icon(Icons.info, color: Color(mainColor),),
                  ),
                  Expanded(
                    child: Text(
                      AppLocalizations.of(context).translate("newItemNote"),
                      style: TextStyle(color: Color(mainColor), fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    textColor: Color(mainColor),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(AppLocalizations.of(context).translate("cancel")),
                  ),
                  FlatButton(
                    textColor: Colors.white,
                    color: Color(mainColor),
                    onPressed: () {
                      _saveShoppingItem(context);
                    },
                    child: Text(AppLocalizations.of(context).translate("save")),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      appBar: CustomAppBar(
          title: AppLocalizations.of(context).translate("newItem")),
    );
  }

  void _saveShoppingItem(BuildContext context) {
    if (_nameController.text.trim().length == 0) {
      showDialog(context: context,
        builder: (context) => AlertDialog(
          title: Text(AppLocalizations.of(context).translate("saveErrorHeader")),
          content: Text(AppLocalizations.of(context).translate("saveErrorText")),
          actions: [
            FlatButton(
              textColor: Color(mainColor),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('OK'),
            ),
          ],
        )
      );
    } else {
      ShoppingItem item = ShoppingItem(
        name: _nameController.text,
        description: _descriptionController.text,
        inCart: false,
        inList: true
      );
      DbService().insertShoppingItem(item);
      Navigator.pop(context);
    }
  }
}
