import 'package:flutter/material.dart';
import 'package:shopping_list/constants/colors.dart';
import 'package:shopping_list/models/shopping_item.dart';
import 'package:shopping_list/services/app_localization.dart';

class CardWidget extends StatelessWidget {
  final ShoppingItem item;
  final VoidCallback itemDeleted;
  final VoidCallback itemInList;

  const CardWidget({Key key, this.item, this.itemDeleted, this.itemInList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
        side: BorderSide(color: Color(mainColor), width: 2),
      ),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Text(
                item.name,
                style: TextStyle(
                  color: Color(mainColor),
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  RawMaterialButton(
                    onPressed: () => {
                      showDialog(context: context,
                          builder: (context) => AlertDialog(
                            title: Text(AppLocalizations.of(context).translate("confirmation")),
                            content: Text(AppLocalizations.of(context).translate("deleteCardConfirmation")),
                            actions: [
                              FlatButton(
                                textColor: Color(mainColor),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text(AppLocalizations.of(context).translate("cancel")),
                              ),
                              FlatButton(
                                textColor: Colors.white,
                                color: Color(mainColor),
                                onPressed: () {
                                  itemDeleted();
                                  Navigator.pop(context);
                                },
                                child: Text('OK'),
                              ),
                            ],
                          )
                      )
                    },
                    elevation: 2.0,
                    fillColor: Colors.white,
                    child: Image(
                      image: AssetImage('assets/images/trash.png'),
                      height: 30,
                    ),
                    padding: EdgeInsets.all(18.0),
                    shape: CircleBorder(),
                  ),
                  RawMaterialButton(
                    onPressed: () => itemInList(),
                    elevation: 2.0,
                    fillColor: Colors.white,
                    child: Row(
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/images/arrow-left.png'),
                          height: 30,
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: Text(
                            AppLocalizations.of(context).translate("toList"),
                            style: TextStyle(color: Color(mainColor)),
                          ),
                        )
                      ],
                    ),
                    padding: EdgeInsets.all(18.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(36.0),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
