import 'package:flutter/material.dart';
import 'package:shopping_list/constants/colors.dart';
import 'package:shopping_list/models/shopping_item.dart';
import 'package:shopping_list/services/app_localization.dart';
import 'package:shopping_list/services/db_service.dart';

import 'item_widget.dart';

class ItemList extends StatelessWidget {
  final VoidCallback itemDeleted;
  final VoidCallback itemInCart;

  const ItemList({Key key, this.itemDeleted, this.itemInCart})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
        child: FutureBuilder<List<ShoppingItem>>(
            future: DbService().itemList(1),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return snapshot.data.length > 0
                    ? ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return ItemWidget(
                            item: snapshot.data[index],
                            itemDeleted: () {
                              ShoppingItem currentItem = snapshot.data[index];
                              ShoppingItem modifiedItem = ShoppingItem(
                                  id: currentItem.id,
                                  name: currentItem.name,
                                  description: currentItem.description,
                                  inList: false,
                                  inCart: false);
                              DbService().insertShoppingItem(modifiedItem);
                              itemDeleted();
                            },
                            itemInCart: () {
                              ShoppingItem currentItem = snapshot.data[index];
                              ShoppingItem modifiedItem = ShoppingItem(
                                  id: currentItem.id,
                                  name: currentItem.name,
                                  description: currentItem.description,
                                  inList: currentItem.inList,
                                  inCart: !currentItem.inCart);
                              DbService().insertShoppingItem(modifiedItem);
                              itemInCart();
                            },
                          );
                        },
                      )
                    : Center(
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppLocalizations.of(context).translate("itemListEmpty"),
                            style: TextStyle(color: Colors.black54, fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      );
              } else if (snapshot.hasError) {
                _showError(context, snapshot);
                return ListView.builder(
                  itemCount: 0,
                  itemBuilder: (context, index) {
                    return Container();
                  },
                );
              }
              // By default, show a loading spinner.
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[CircularProgressIndicator()],
                ),
              );
            }),
      ),
    );
  }

  Widget _showError(context, snapshot) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Scaffold.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(new SnackBar(content: Text(snapshot.error.toString())));
    });
    debugPrint(snapshot.error.toString());
    return Container();
  }
}
