import 'package:flutter/material.dart';
import 'package:shopping_list/models/shopping_item.dart';
import 'package:shopping_list/services/app_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shopping_list/widgets/app_bar.dart';
import 'package:shopping_list/widgets/card_list.dart';
import 'package:shopping_list/widgets/item_list.dart';
import 'package:shopping_list/widgets/new_item.dart';
import 'package:shopping_list/widgets/settings.dart';

import 'constants/colors.dart';

void main() {
  runApp(ShoppingListApp());
}

class ShoppingListApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: [
        Locale('en', 'US'),
        Locale('ru', ''),
      ],
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      theme: ThemeData(
        primarySwatch: MaterialColor(mainColor, color),
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int itemIndex = 0;
  List<ShoppingItem> shoppingItems = List<ShoppingItem>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: itemIndex == 0
          ? ItemList(
              itemDeleted: () => {setState(() {})},
              itemInCart: () => {setState(() {})},
            )
          : itemIndex == 1
              ? CardList(
                  itemDeleted: () => {setState(() {})},
                  itemInList: () => {setState(() {})},
                )
              : itemIndex == 2
                  ? Settings()
                  : ItemList(
                      itemDeleted: () => {setState(() {})},
                      itemInCart: () => {setState(() {})},
                    ),
      appBar: CustomAppBar(title: _getTitle(itemIndex, context)),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: itemIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.format_align_left, size: 25),
              title: Text(""),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.filter, size: 25),
              title: Text(""),
            ),
            /*
            BottomNavigationBarItem(
              icon: Icon(
                Icons.brightness_7,
                size: 25,
              ),
              title: Text(""),
            ),

             */
          ],
          onTap: (int index) {
            setState(() {
              itemIndex = index;
            });
          }),
      floatingActionButton: itemIndex == 0
          ? Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                // TODO: Add clear button
                // Add item button
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 15, 15),
                  child: Container(
                    width: 60.0,
                    height: 60.0,
                    child: RawMaterialButton(
                      fillColor: Color(mainColor),
                      shape: new CircleBorder(),
                      elevation: 10.0,
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 30,
                      ),
                      onPressed: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => NewItem()))
                            .then((value) => setState(
                                  () {},
                                ));
                      },
                    ), // This trailing comma makes auto-formatting nicer for build methods.
                  ),
                ),
              ],
            )
          : null,
    );
  }

  static String _getTitle(int itemIndex, BuildContext context) {
    switch (itemIndex) {
      case 0:
        {
          return AppLocalizations.of(context).translate("list");
        }
      case 1:
        {
          return AppLocalizations.of(context).translate("cards");
        }
      case 2:
        {
          return AppLocalizations.of(context).translate("settings");
        }
      default:
        {
          return "";
        }
    }
  }
}
