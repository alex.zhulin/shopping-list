class ShoppingItem {
  final int id;
  final String name;
  final String description;
  final bool inList;
  final bool inCart;

  ShoppingItem({this.id, this.name, this.description, this.inList, this.inCart});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'inList': inList,
      'inCart': inCart,
    };
  }
}
