// Main application color
import 'dart:ui';

// Colors
const int mainColorDark = 0xFF25C685;
const int mainColor = 0xFF3DD598;
const int mainColorLight = 0xE08aa6c6;
const int background = 0xFF2A3C44;
const int disableColor = 0xFF919399;
const int greyColor = 0xFF929292;

const Map<int, Color> color =
{
  50: const Color(mainColor),
  100: const Color(mainColor),
  200: const Color(mainColor),
  300: const Color(mainColor),
  400: const Color(mainColor),
  500: const Color(mainColor),
  600: const Color(mainColor),
  700: const Color(mainColor),
  800: const Color(mainColor),
  900: const Color(mainColor),
};
