import 'package:path/path.dart';
import 'package:shopping_list/models/shopping_item.dart';
import 'package:sqflite/sqflite.dart';

class DbService {
  Future<Database> _openDatabase() async {
    return openDatabase(
      // Set the path to the database. Note: Using the `join` function from the
      // `path` package is best practice to ensure the path is correctly
      // constructed for each platform.
      join(await getDatabasesPath(), 'shopping.db'),
      // When the database is first created, create a table for shopping list.
      onCreate: (db, version) {
        db.execute(
            "CREATE TABLE shopping_item(id INTEGER PRIMARY KEY, name TEXT, description TEXT, inList INTEGER, inCart INTEGER);"
        );
        db.execute(
            "create unique index i_item_name on shopping_item (name);;"
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );
  }

  Future<void> insertShoppingItem(ShoppingItem shoppingItem) async {
    final Database db = await _openDatabase();
    await db.insert(
      'shopping_item',
      shoppingItem.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteShoppingItem(int itemId) async {
    final Database db = await _openDatabase();
    await db.delete(
      'shopping_item',
      where: "id = ?",
      whereArgs: [itemId],
    );
  }

  Future<List<ShoppingItem>> itemList(int inList) async {
    final Database db = await _openDatabase();
    final List<Map<String, dynamic>> maps = await db.query('shopping_item', where: "inList = ?", whereArgs: [inList], orderBy: "inCart, name");
    return List.generate(maps.length, (i)
    {
      return ShoppingItem(
        id: maps[i]["id"],
        name: maps[i]["name"],
        description: maps[i]["description"],
        inList: maps[i]["inList"] == 1,
        inCart: maps[i]["inCart"] == 1,
      );
    });
  }

}